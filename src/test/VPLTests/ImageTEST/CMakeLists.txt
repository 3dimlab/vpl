#==============================================================================
# This file comes from MDSTk software and was modified for
#
# VPL - Voxel Processing Library
# Changes are Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# The original MDSTk legal notice can be found below.
# 
# Medical Data Segmentation Toolkit (MDSTk)
# Copyright (c) 2007 by PGMed@FIT
#
# Authors: Miroslav Svub, svub@fit.vutbr.cz
#          Michal Spanel, spanel@fit.vutbr.cz
# Date:    2007/08/25
#==============================================================================

set( VPL_TESTS
     ImageAccessTEST
     VolumeAccessTEST )

foreach( VPL_TEST ${VPL_TESTS} )

  add_executable( ${VPL_TEST} ${VPL_TEST}.cpp )
  target_link_libraries( ${VPL_TEST}
                         ${VPL_LIBS}
						 ${VPL_ZLIB}
                         ${VPL_SYSTEM_LIBS} )
  set_target_properties( ${VPL_TEST} PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )
    INSTALL( TARGETS ${VPL_TEST}
    RUNTIME DESTINATION test/vpl/Image )

endforeach( VPL_TEST )


if( VPL_OPENCV_ENABLED )

  add_executable( CvImageTEST CvImageTEST.cpp )
  target_link_libraries( CvImageTEST
                         ${VPL_LIBS}
						 ${VPL_ZLIB}
                         ${VPL_SYSTEM_LIBS} 
                         ${VPL_OPENCV} )
  set_target_properties( CvImageTEST PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )
                         
       INSTALL( TARGETS CvImageTEST
    RUNTIME DESTINATION test/vpl/Image )
endif( VPL_OPENCV_ENABLED )
