//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#if defined _M_X64

#include "gtest/gtest.h"
#include <VPL/Math/Vector.h>
#include <VPL/Math/TransformMatrix.h>
#include <VPL/Math/Quaternion.h>
#include <VPL/Math/MatrixFunctions.h>
#include <VPL/Test/Compare/compare2D.h>
#include <VPL/Test/Compare/compare1D.h>
#include <VPL/Test/printDefinition.h>
#include "gtest/gtest-typed-test.h"


namespace staticMatrix
{
template <class T>
class StaticMatrixTest : public testing::Test
{
public:
    typedef  T type;

    vpl::math::CStaticMatrix<type, 3, 3> m1;
    vpl::math::CStaticMatrix<type, 3, 3> m2;

    vpl::test::Compare2D<type, vpl::math::CStaticMatrix<type, 3, 3>> compare2D;

	vpl::test::Compare1D<type, vpl::math::CStaticVector<type, 3>> compare1D;


    void SetUp() override
    {
        compare2D.setErrorMessage("Matrix is different at index");
        compare2D.setAccessType(vpl::test::AccessType::YX_ACCESS);
        compare2D.enablePrintData(true);

        m1.unit();
        generateSequence(m2);
    }
    void generateSequence(vpl::math::CStaticMatrix<type, 3, 3>& m)
    {
        
        for (vpl::tSize i = 0; i < m.getNumOfRows(); i++)
        {
            for (vpl::tSize j = 0; j < m.getNumOfCols(); j++)
            {
                m(i, j) = i * m.getNumOfRows() + j;
            }
        }
    }
    void initializeMatrixType1(vpl::math::CStaticMatrix<type, 3, 3>& m)
    {
        m(0, 0) = 2; m(0, 1) = 1; m(0, 2) = 0; m(0, 3) = 0; m(0, 4) = 0;
        m(1, 0) = 1; m(1, 1) = 2; m(1, 2) = 1; m(1, 3) = 0; m(1, 4) = 0;
        m(2, 0) = 0; m(2, 1) = 1; m(2, 2) = 2; m(2, 3) = 1; m(2, 4) = 0;
        m(3, 0) = 0; m(3, 1) = 0; m(3, 2) = 1; m(3, 3) = 2; m(3, 4) = 1;
        m(4, 0) = 0; m(4, 1) = 0; m(4, 2) = 0; m(4, 3) = 1; m(4, 4) = 2;
    }
};

typedef testing::Types< double, float> implementations;
TYPED_TEST_CASE(StaticMatrixTest, implementations);


//! Testing unit initialize of matrix, and generate sequence (0,1,2,3,...)
TYPED_TEST(StaticMatrixTest, Initialize)
{
	using type = typename TestFixture::type;

    type test[] = { 1,0,0, 0,1,0, 0,0,1 };
    TestFixture::compare2D.values(test, TestFixture::m1, 3, 3);

    TestFixture::compare2D.sequence(TestFixture::m2, 3, 3, 0, 1);

}
//! Testing operator +=
TYPED_TEST(StaticMatrixTest,OperatorAdd)
{
	using type = typename TestFixture::type;

	TestFixture::m1 += TestFixture::m2;
	type test[] = { 1,1,2, 3,5,5, 6,7,9 };

	TestFixture::compare2D.values(test, TestFixture::m1,3,3);
}

//! Testing operator *=, and mult(m1, m2)
TYPED_TEST(StaticMatrixTest, OperatorMultiply)
{
	using type = typename TestFixture::type;

	TestFixture::m2 *= vpl::CScalar<int>(2);

	type test[] = { 0,2,4, 6,8,10, 12,14,16 };

	TestFixture::compare2D.values(test, TestFixture::m2, 3, 3);

	TestFixture::generateSequence(TestFixture::m1);
	TestFixture::generateSequence(TestFixture::m2);

	vpl::math::CStaticMatrix<type, 3, 3> m3;
	m3.mult(TestFixture::m1, TestFixture::m2);

	type test2[] = { 15,18,21, 42,54,66, 69,90,111 };
	TestFixture::compare2D.values(test2, m3, 3, 3);
}
//! Testing multiply matrix and vector
TYPED_TEST(StaticMatrixTest, OperatorWithVector)
{
	using type = typename TestFixture::type;

	TestFixture::m1 += TestFixture::m2;
	TestFixture::m1 += TestFixture::m2;
	type test[] = { 1,2,4, 6,9,10, 12,14,17 };
	TestFixture::compare2D.values(test, TestFixture::m1, 3, 3);

	vpl::math::CStaticVector<type, 3> v1;
	for (vpl::tSize j = 0; j < v1.getSize(); j++)
	{
		v1(j) = j;
	}
	vpl::math::CStaticVector<type, 3> v2;

	v2.mult(TestFixture::m1, v1);
	type test2[] = { 10,29,48 };
	TestFixture::compare1D.values(test2, v2, 3);

	v2.mult( v1, TestFixture::m1);
	type test3[] = { 30,37,44 };
	TestFixture::compare1D.values(test3, v2, 3);
}
}
#endif