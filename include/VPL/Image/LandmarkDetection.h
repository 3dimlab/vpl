//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2008 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2007/07/03                       
 *
 * Description:
 * - Predefined volume landmark detectors.
 */

#ifndef VPL_LandmarkDetection_H
#define VPL_LandmarkDetection_H


//==============================================================================
/*
 * Include all predefined detectors.
 */

// 3D corner/landmark detectors
#include "LandmarkDetection/Rohr.h"
#include "LandmarkDetection/Susan.h"


#endif // VPL_LandmarkDetection_H

