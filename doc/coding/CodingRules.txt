================================================================================
- This file comes from MDSTk software and was modified for
- 
- VPL - Voxel Processing Library
- Changes are Copyright 2014 3Dim Laboratory s.r.o.
- All rights reserved.
- 
- Use of this file is governed by a BSD-style license that can be
- found in the LICENSE file.
- 
- The original MDSTk legal notice can be found below.
-
- Medical Data Segmentation Toolkit (MDSTk)
- Copyright (c) 2003-2007 by Michal Spanel, FIT, Brno University of Technology
-
- Authors:  Michal Spanel (spanel@fit.vutbr.cz)
- Date:     2003/12/24
================================================================================

Programmer's Coding Rules
-------------------------
This file contains several rules that make your code more effective and more
readable for others.

[C Programming]


[C++ and Templates Design Rules]


[STL Library Usage]

(1) Operators i++ and ++i
    Do not use postfix operator like i++ because it creates a temporary object.
    Instead prefer the prefix notation ++i which might be much faster.

    Example:
        // Slower version
        for( ... ; ... ; i++ )
        {
            ...
        }
        // Faster version
        for( ... ; ... ; ++i )
        {
            ...
        }
 
(2) Cyclic Testing of a Container End
    Method end() also creates a temporary object which makes the usage
    of it ineffective. Instead of a cyclic testing like

        for( list<T>::iterator i = l.begin(); i != i.end(); ++i )
        {
            ...
        }

    prefer

        list<T>::const_iterator iEnd = l.end();
        for( list<T>::iterator i = l.begin(); i != iEnd; ++i )
        {
            ...
        }


[Naming Convention and Source Code Structure]

Description of the VPL library specific classes, class members, variables,
functions and etc. naming convention. Used C/C++ statements layout is described
in appended code templates that can be also used by a source code editor.

(1) Namespace
    All definitions in scope of VPL are located inside the global
    namespace called 'vpl'. There are also many others minor namespaces named
    according to scope of the definition. 

    Example:
        namespace vpl
        {
        namespace img
        {
        ...
        class CImage;
        ...
        } // namespace img
        } // namespace vpl

(2) Classes, Structures and Type Definitions
    Various prefixes are used to introduce classes, structures and so on.
    E.g. name of a class has the prefix 'C'. Complete list can be seen below.

        Type    Prefix
        ----    ------
        class   'C'ClassName
        struct  'S'StructureName
        enum    'E'EnumName
        typedef 't'NewType

(3) Global Functions Naming Convention
    Function name starts with a lower case and following words start with
    a capital. Special kind of functions which return a value starts with 'get'
    and the inverse function (sets value of some member variable) uses prefix
    'set'.

    Example:
        const std::string& getModuleName();
        void initModule();

(4) Global Variables and Function Parameters
    Each variable name consist of words separated by a capital letter.
    In addition, variable name is introduced by a prefix which describes
    its type.
    
        Type                Prefix
        ----                ------
        char                'c'VarName
        int, short, long    'n'VarName  
        unsigned int, ...   'u'VarName
        float, double       'f'VarName
        enum                'e'VarName

        char *              'sz'VarName
        wchar_t *           'wsz'VarName
        std::string         'ss'VarName
        std::wstring        'wss'VarName
        
        pointer type        'p'VarName
        void *              'pv'VarName
        char *              'pc'VarName
        int *               'pn'VarName
        double *            'pf'VarName
        smart pointer type  'sp'VarName

        class                VarName
        structure            VarName
        unspecified type     VarName

(5) Class and Structure Members
    Methods and member variables use same convention as global functions and
    variables. All member variables start with extra prefix 'm_' or just '_'.

    Example:
        namespace vpl
        {
        namespace img
        {
        
        class CImage
        {
            public:
                tPixel getPixel(const int x, const int y);
                setPixel(const int x, const int y, const tPixel Value);
                draw();
                createSubImage();
            private:
                tPixel   *m_pData;
                tSize    m_XSize;
                int      m_iMargin;
        };
        
        } // namespace img
        } // namespace vpl

(6) Constats and Macros
    Name of any constant or macro contains capital letters only. All words
    within the name are separated using '_' character. In addition, the macro
    name starts with the prefix 'VPL_'.

    Example:
        namespace vpl
        {
        
        #define         VPL_MIN(a, b)   (((a) < (b) ? (a) : (b))
        const int       MAX             = 10;
        const tSize     LENGTH          = 20;
        
        } // namespace vpl

(7) Code Templates
    See appended source code templates. These templates define a structure
    of header and source files, including typical VPL file header. There is
    sample code to write a new class and it's commentation. Also the notation
    of C/C++ language statements like if-else, for() and etc. are defined.

(8) Tabulators
    Please, adjust your source code editor to replace all tabs with spaces.
    This restriction prevents source code indentation changes. Default
    tabulator size is 4 characters. 

[Documentation]

Just a one important rule: Please, write a heavily commented code. Extra
documentation is not required but recommended. It can be used to explain
an module usage or some algorithm details.

(1) Source Code Comments
    Every global variable, constant, macro, class and class member must
    be commented. Two comment notations could be used. First, each comment
    line starts with '//!' characters instead of classical '//'. Second,
    a comment is enclosed between '/*!' and '*/' characters. Exclamation mark
    is required, because source code documentation is automatically generated
    using Doxygen tool. This is a special Doxygen comment notation.

    Example:
        namespace vpl
        {
        namespace my
        {
               
        //======================================================================
        /*
         * Global definitions.
         */
        
        //! Constant description
        const int MY_CONST = 0;
        
        //======================================================================
        /*!
         * Class description.
         */
        class CMyClass
        {
        public:
            //! Class member description
            //! - Notes
            ...
        };
        
        } // namespace my
        } // namespace vpl


================================================================================
